import apiClient from './axios';

export const getListPhotos = async () => {
	const response = await apiClient.get(`list-images`, {
		headers: {
			'Content-Type': 'application/json'
		},
	});
	return response;
};
