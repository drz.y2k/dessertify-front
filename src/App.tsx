import { ChakraProvider } from '@chakra-ui/react';
import { Main } from './components/Main';
import theme from './theme';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const queryClient = new QueryClient();


function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<ChakraProvider theme={theme}>
				<Main />
			</ChakraProvider>
		</QueryClientProvider>
	);
}

export default App;

