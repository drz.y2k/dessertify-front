import { defineStyle, defineStyleConfig } from '@chakra-ui/react'
const xxxl = defineStyle({
  height: 200,
  width: 200,
});
export const spinnerTheme = defineStyleConfig({
  sizes: { xxxl },
})