import { useState } from 'react';
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

import { useToast, Box } from '@chakra-ui/react';

import {  useQueryClient } from '@tanstack/react-query';

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

export const FileLoader = () => {
	const queryClient = useQueryClient();
	const [files, setFiles] = useState<any[]>([]);
	const toast = useToast();
	return (
		<div className="App">
			<FilePond
				// instantUpload={false}
				allowRevert={false}
				files={files}
				onupdatefiles={(e) => {
					setFiles(e);
					
				}}
				allowMultiple={false}
				server={`${import.meta.env.VITE_BASE_URL}upload-image`}
				acceptedFileTypes={['image/png', 'image/jpeg']}
				name="myfile" /* sets the file input name, it's filepond by default */
				labelIdle='Drag & Drop your files or <span class="filepond--label-action">browse</span>'
				onprocessfile={(err) => {
					setTimeout(() => setFiles([]), 1800);
					if (err !== null) {
						toast({
							position: 'top',
							status: 'error',
							isClosable: true,
							title: `Error: According to AWS Rekognition the uploaded file is not a dessert or contains forbidden content.`,
						});
					}else{
						
					}
				}}
				onprocessfiles={
					() => {
						queryClient.invalidateQueries(['list-photos']);
					}
				}
			/>
		</div>
	);
};

