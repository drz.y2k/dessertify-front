import {
	useColorMode,
	Container,
	Box,
	GridItem,
	Spinner,
	defineStyle,
	defineStyleConfig,
	Center,
	Text,
} from '@chakra-ui/react';
import { CardComponent } from './CardComponent';
import { FileLoader } from './FileLoader';
import { useQuery } from '@tanstack/react-query';
import { getListPhotos } from '../axios/api';
import { useState } from 'react';

const xxl = defineStyle({
	height: 1000,
	width: 1000,
});
export const spinnerTheme = defineStyleConfig({
	sizes: { xxl },
});

export const Main = () => {
	const { colorMode, toggleColorMode } = useColorMode();
	const [listPhotos, setListPhotos] = useState<any>({ data: [] });

	const queryListPhotos = useQuery({
		staleTime: 60000,
		queryKey: ['list-photos'],
		queryFn: () => {
			return getListPhotos();
		},
		onSuccess: (photos: any) => {
			setListPhotos(photos);
		},
		onError: (error) => {},
	});

	return (
		<>
			{/* {JSON.stringify(listPhotos.data[1])} */}
			<Container marginTop={'20px'} minW="70%">
				<h1 className="custom-font">Dessertify</h1>
				<Text
					fontSize={{ base: '11px',sm:'18px', md: '25px' }}
					p={4}
					fontWeight="bold"
					textAlign={'center'}
				>
					Select an image, the AWS Rekognition AI will detect if it is
					a dessert and if it is appropriate for the platform. If the
					image is not a dessert or contains forbidden content, it
					will be deleted from the server. A maximum of 12 images can
					be stored at the same time.
				</Text>
				{/* {colorMode}
				<Button size="sm" colorScheme="blue" onClick={toggleColorMode}>
					Toggle Mode
				</Button> */}

				<FileLoader />
				{listPhotos.data?.length === 0 ? (
					<Center m={6}>
						<Spinner size="xxxl"></Spinner>
					</Center>
				) : (
					<Box
						sx={{
							// '@media print': {
							//   display: 'none',
							// },
							columns: {
								base: 1,
								md: 2,
								xl: 4,
							},
							columnGap: '0.5em',
							columnWidth: '100px',
						}}
					>
						{listPhotos.data.map((photo: any) => {
							return (
								<GridItem
									width="100%"
									marginBottom={'0.5em'}
									key={photo._id}
								>
									<CardComponent image={photo.signedURL} />
								</GridItem>
							);
						})}
					</Box>
				)}
			</Container>
		</>
	);
};

