import {
	Card,
	Image,
} from '@chakra-ui/react';

export const CardComponent = ({ image }: any) => {
	return (
		<Card maxW="lg" borderRadius={'lg'}>
			<Image
				src={image}
				alt="Dessert"
				borderRadius="lg"
				outline={'2px solid #fff'}
				outlineOffset="-12px"
			/>
		</Card>
	);
};

