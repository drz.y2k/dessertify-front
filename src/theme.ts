// theme.ts

// 1. import `extendTheme` function
import { extendTheme, type ThemeConfig } from '@chakra-ui/react';
import { spinnerTheme } from './custom-themes/spinnerTheme';

// 2. Add your color mode config
const config: ThemeConfig = {
	initialColorMode: 'dark',
	useSystemColorMode: false,
};

// 3. extend the theme
const theme = extendTheme({
	config,
	components: {
		Spinner: spinnerTheme,
	},
});

export default theme;

